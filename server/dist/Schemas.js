"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.State = exports.Quat = exports.Vect3 = exports.DoorData = exports.LightData = exports.BallData = exports.PlayerData = exports.EntityData = void 0;
const schema_1 = require("@colyseus/schema");
class Vect3 extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
}
__decorate([
    (0, schema_1.type)('float32')
], Vect3.prototype, "x", void 0);
__decorate([
    (0, schema_1.type)('float32')
], Vect3.prototype, "y", void 0);
__decorate([
    (0, schema_1.type)('float32')
], Vect3.prototype, "z", void 0);
exports.Vect3 = Vect3;
class Quat extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.w = 1;
    }
}
__decorate([
    (0, schema_1.type)('float32')
], Quat.prototype, "x", void 0);
__decorate([
    (0, schema_1.type)('float32')
], Quat.prototype, "y", void 0);
__decorate([
    (0, schema_1.type)('float32')
], Quat.prototype, "z", void 0);
__decorate([
    (0, schema_1.type)('float32')
], Quat.prototype, "w", void 0);
exports.Quat = Quat;
class EntityData extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.position = new Vect3();
        this.rotation = new Quat();
    }
}
__decorate([
    (0, schema_1.type)(Vect3)
], EntityData.prototype, "position", void 0);
__decorate([
    (0, schema_1.type)(Quat)
], EntityData.prototype, "rotation", void 0);
exports.EntityData = EntityData;
class PlayerData extends EntityData {
    constructor() {
        super(...arguments);
        this.name = null;
        this.color = 0;
        this.emote = 0;
        // Used only server-side for convenience, not synchronized with clients
        this.ownedBall = '';
    }
}
__decorate([
    (0, schema_1.type)('string')
], PlayerData.prototype, "name", void 0);
__decorate([
    (0, schema_1.type)('uint8')
], PlayerData.prototype, "color", void 0);
__decorate([
    (0, schema_1.type)('uint8')
], PlayerData.prototype, "emote", void 0);
exports.PlayerData = PlayerData;
class BallData extends EntityData {
    constructor() {
        super(...arguments);
        this.owner = '';
    }
}
__decorate([
    (0, schema_1.type)('string')
], BallData.prototype, "owner", void 0);
exports.BallData = BallData;
class LightData extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.on = true;
    }
}
__decorate([
    (0, schema_1.type)('boolean')
], LightData.prototype, "on", void 0);
exports.LightData = LightData;
class DoorData extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.open = false;
    }
}
__decorate([
    (0, schema_1.type)('boolean')
], DoorData.prototype, "open", void 0);
exports.DoorData = DoorData;
class State extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.players = new schema_1.MapSchema();
        this.lights = new LightData();
        this.doors = new DoorData();
        this.balls = new schema_1.MapSchema();
    }
}
__decorate([
    (0, schema_1.type)({ map: PlayerData })
], State.prototype, "players", void 0);
__decorate([
    (0, schema_1.type)(LightData)
], State.prototype, "lights", void 0);
__decorate([
    (0, schema_1.type)(DoorData)
], State.prototype, "doors", void 0);
__decorate([
    (0, schema_1.type)({ map: BallData })
], State.prototype, "balls", void 0);
exports.State = State;
