"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const colyseus_1 = require("colyseus");
// import {monitor} from '@colyseus/monitor';
// import socialRoutes from '@colyseus/social/express';
const ChatRoom_1 = require("./ChatRoom");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const apiLimiter = (0, express_rate_limit_1.default)({
    windowMs: 15 * 60 * 1000,
    max: 100
});
const port = Number(process.env.PORT || 2567);
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use("/matchmake/", apiLimiter);
app.set('trust proxy', 1);
const server = http_1.default.createServer(app);
const gameServer = new colyseus_1.Server({
    server,
});
gameServer.define('lobbyroom', colyseus_1.LobbyRoom);
// register your room handlers
gameServer.define('chatroom', ChatRoom_1.ChatRoom).enableRealtimeListing();
/**
 * Register @colyseus/social routes
 *
 * - uncomment if you want to use default authentication (https://docs.colyseus.io/authentication/)
 * - also uncomment the import statement
 */
// app.use('/', socialRoutes);
// register colyseus monitor AFTER registering your room handlers
// app.use('/colyseus', monitor());
gameServer.listen(port);
console.log(`Listening on ws://localhost:${port}`);
