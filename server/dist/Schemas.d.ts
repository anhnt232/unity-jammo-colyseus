import { Schema, MapSchema } from '@colyseus/schema';
declare class Vect3 extends Schema {
    x: number;
    y: number;
    z: number;
}
declare class Quat extends Schema {
    x: number;
    y: number;
    z: number;
    w: number;
}
declare class EntityData extends Schema {
    position: Vect3;
    rotation: Quat;
}
declare class PlayerData extends EntityData {
    name: string;
    color: number;
    emote: number;
    ownedBall: string;
}
declare class BallData extends EntityData {
    owner: string;
}
declare class LightData extends Schema {
    on: boolean;
}
declare class DoorData extends Schema {
    open: boolean;
}
declare class State extends Schema {
    players: MapSchema<PlayerData>;
    lights: LightData;
    doors: DoorData;
    balls: MapSchema<BallData>;
}
export { EntityData, PlayerData, BallData, LightData, DoorData, Vect3, Quat, State };
