import { Room, Client } from 'colyseus';
import { PlayerData, Vect3, Quat } from './Schemas';
import { World, Body, Material } from 'cannon-es';
declare class PlayerInput {
    position: Vect3;
    rotation: Quat;
}
declare class Area {
    minX: number;
    maxX: number;
    minZ: number;
    maxZ: number;
    minRY: number;
    maxRY: number;
    constructor(minX: number, maxX: number, minZ: number, maxZ: number, minRY?: number, maxRY?: number);
}
declare class SphereMap {
    [name: string]: Body;
}
export declare class ChatRoom extends Room {
    world: World;
    spheres: SphereMap;
    groundMaterial: Material;
    northWallMaterial: Material;
    southWallMaterial: Material;
    eastWallMaterial: Material;
    westWallMaterial: Material;
    onCreate(options: any): void;
    onJoin(client: Client, options: any): void;
    onLeave(client: Client): void;
    onDispose(): void;
    update(delta: number): void;
    createFloor(): void;
    createWalls(): void;
    onPlayerMove(client: Client, playerInput: PlayerInput): void;
    onPlayerEmote(client: Client, emote: number): void;
    onPlayerInteract(client: Client): void;
    tryCatchBall(client: Client): boolean;
    tryOperateLights(client: Client): boolean;
    tryOperateDoors(client: Client): boolean;
    trySpawnBall(client: Client): boolean;
    tryThrowBall(client: Client): boolean;
    getReachableClosestBallId(player: PlayerData): string;
    refreshBallsSpacialization(): void;
    isPlayerInArea(player: PlayerData, area: Area): boolean;
}
export {};
