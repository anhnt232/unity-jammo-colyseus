using Cinemachine;
using UnityEngine;
[RequireComponent(typeof(CinemachineFreeLook))]
public class FreeLookUserInput : MonoBehaviour
{

    private CinemachineFreeLook freeLookCam;

    protected Joystick camJoystick;

    // Use this for initialization
    void Start()
    {
        freeLookCam = GetComponent<CinemachineFreeLook>();

        camJoystick = GameObject.FindWithTag("camJoystick").GetComponent<Joystick>();
    }

    // Update is called once per frame
    void Update()
    {
        freeLookCam.m_XAxis.m_InputAxisValue = camJoystick.Horizontal;
        freeLookCam.m_YAxis.m_InputAxisValue = camJoystick.Vertical;

    }
}
