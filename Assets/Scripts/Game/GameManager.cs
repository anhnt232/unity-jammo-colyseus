﻿using Colyseus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AvatarMoveEvent : UnityEvent<Vector3, Quaternion> { };
public class AvatarEmoteEvent : UnityEvent<int> { };
public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;

    public AvatarMoveEvent onAvatarMove = new();
    public AvatarEmoteEvent onAvatarEmote = new ();
    [HideInInspector]
    public UnityEvent onAvatarInteract = new ();

    private Avatar avatar;
    private readonly Dictionary<string, Player> players = new();

    private readonly float sendInterval = 50 / 1000f;
    private float sendTimer = 0f;


    // Start is called before the first frame update
    void Start()
    {
        avatar = transform.Find("Avatar").GetComponent<Avatar>();
        avatar.transform.position = new Vector3(270, 1, 192);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            avatar.ChangeEmote(0);
            onAvatarEmote.Invoke(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            avatar.ChangeEmote(1);
            onAvatarEmote.Invoke(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            avatar.ChangeEmote(2);
            onAvatarEmote.Invoke(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            avatar.ChangeEmote(3);
            onAvatarEmote.Invoke(3);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            onAvatarInteract.Invoke();
        }
        sendTimer += Time.deltaTime;

        if (sendTimer >= sendInterval)
        {
            sendTimer = 0f;
            onAvatarMove.Invoke(avatar.transform.position, avatar.transform.rotation);
        }
    }
    public void SetAvatarColor(int color)
    {
        avatar.ChangeColor(color);
    }

    public void AddPlayer(string id, PlayerData pd)
    {
        Player newPlayer = Instantiate(playerPrefab, transform).GetComponent<Player>();
        newPlayer.transform.SetPositionAndRotation(new Vector3(pd.position.x, pd.position.y, pd.position.z), new Quaternion(pd.rotation.x, pd.rotation.y, pd.rotation.z, pd.rotation.w));
        newPlayer.SetPositionTarget(new Vector3(pd.position.x, pd.position.y, pd.position.z));
        newPlayer.SetRotationTarget(new Quaternion(pd.rotation.x, pd.rotation.y, pd.rotation.z, pd.rotation.w));
        newPlayer.SetName(pd.name);
        newPlayer.SetColor((int)pd.color);
        players.Add(id, newPlayer);
    }

    public void RemovePlayer(string id, PlayerData pd)
    {
        Player leftPlayer = players[id];
        Destroy(leftPlayer.gameObject);
        players.Remove(id);
    }

    public void PlayerMoved(string id, Vector3 position)
    {
        Player player = players[id];
        Vector3 targetPos = new Vector3(position.x, position.y, position.z);
        if (Vector3.Distance(targetPos, player.transform.position) > 0.01f)
        {
            player.SetPositionTarget(targetPos);
        }
    }
    public void PlayerRotated(string id, Quaternion rotation)
    {
        Player player = players[id];
        if (Vector3.Distance(rotation.eulerAngles, player.transform.eulerAngles) > 0.01f)
        {
            player.SetRotationTarget(rotation);
        }
    }
    public void PlayerChangedEmote(string id, int emote)
    {
        Player player = players[id];
        if (emote != player.GetEmote())
        {
            player.SetEmote(emote);
        }
    }
}
